[查看简体中文说明书&gt;](#简体中文手册)   
[查看测试网页&gt;](http://musamdu.tk/button-frame/)   
[View the English Manual&gt;](#english-manual)   
[View DEMO Page&gt;](http://musamdu.tk/button-frame/)   

#English Manual
##Introduction
These are button frames which i made for web.Of course, you can use them for other cases, but they're designed to suit CSS3 *border-image* feature.   
   
The source files are in the *"sources"* folder. They're created with [Inkscape](inkscape.org), and also manually fixed. The source files are in the [SVG](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) fromat, so ther're scalable; That mean these images can be as large as you want, and of course as small as you will, and the quality of those images won't change.   
   
The colors i use are following Google's [Material Design](https://design.google.com/) standard. I'm choosing all the 20 main colors. You can find the Google's official full color pattle at [it's website](https://www.google.com/design/spec/style/color.html#color-color-palette).   
   
These sources' default size is 12cm x 12cm, and each block inside are 1cm x 1cm, so if you don't want to spend time on initializeing the images, you may like to render a bitmap image yourself, or you can use my rendered PNG files.   

##Downloading
You have 2 methods to do this.   

####Meth #1
Go ahead to the [release](https://github.com/MuSamDu/button-frame/releases) page like this:   
![Switch To Release Page](switch-to-release-page.png)   
I use 2 tags for each release. They are:   
   
>V#   
>V#-DEMO-PAGE   
   
The *#* above should be replaced by the version number.    
The *V#* tag is used to release the master branch, including SVG source files, PNG bitmaps, the styling file, the license files, and this readme.   
The *V#-DEMO-PAGE* tag is used to release the demo page. Scroll the page down for more info.   
   
Find the newest version number, then choose whatever you want, you'll see two files under each tag:   
![Files](files.png)   
Choose the file fromat that you prefer, click on it, the download should start soon.   

####Meth #2
There are two branches under this repo. The *master* branch, and the *gh-pages* branch.   
The *master* branch included the SVG source files, PNG bitmaps, the styling file, the license files, and this readme.   
The *gh-pages* branch included the full repo of the demo page. Scroll the page down for more info.   
Choose the branch like **[This](https://github.com/MuSamDu/button-frame#5head-up)** .   
Then you can download the things you want.   

##Using
*These instructions are only my suggestions, of course you can use them how-ever you want. But you HAVE TO follow the license. Scroll down for more info.*   

####1.Initialize
Copy my PNG folder to the same directory as the html file you're gonna use, give it a name *"borders"*.   
   
If you rendered your own images, do the same thing, put them into the "borders" folder which is at the same folder as your html file. Then copy the "borders.css" from my PNG folder and put it together with your images. **But, name the images AS WHAT I DID to mine, or it may NOT WORK properly.**   
   
Also, copy the *"LICENSE"* file and the *"LICENSE.html"* to the same directory with the images. You **Must** do so to follow the license. Scroll down for more info about the license.   

####2.Setup
Add these code before your *&lt;/head&gt;* tag:   
   
    <link type="text/css" rel="stylesheet" href="borders/borders.css"/>
   
If you're using your own images, you might need to do some customizations with *"borders.css"* in the *"borders"* directory.   
I sat the *border-width* into 10px, of course you can change it, but better don't do it with my PNGs.   

####3.Configuration
You have 2 ways to do this.   
The first way:   
create element:   
   
    <style>

    </style>
   
before your *&lt;/head&gt;* tag.   
*If you did so just ignore.*   
Then paste the following code between *&lt;style&gt;* and *&lt;/style&gt;* :   
   
    .button-borderd{
        display: inline-block;
        margin: 10px;
        padding: 10px;
        text-decoration: none;
        background-color: #fff;
        box-shadow: 0px 2px 9px 2px #666666;
        transition: 0.3s;
    }

    .button-borderd:hover{
        text-decoration: none;
        box-shadow: 0px 2px 14px 3px #666666;
        transform: translate(0px,-1px);
    }

    .button-borderd:active{
        text-decoration: none;
        box-shadow: 0px 0px 3px 1px #666666;
        transform: translate(0px,3px);
    }
   
Or the second way:   
Create a file called *"buttons.css"* or open your recent styling file and add the code above to the end.   
To make the code work, you need to paste the following code before your *&lt;/head&gt;* tag:   
   
    <link type="text/css" rel="stylesheet" href="buttons.css"/>
   
If you added the css code to the end of your styling file, you might have done this, so don't do it again; But if you didn't, chage the value of *href* from the code above into your styling file's path.   
   
**If you SURE you know how**, you can modify these code to suit your needs, and make your buttons more fancy.   

####4.Create Buttons!!!
Go to your html file then paste the following code to wherever you want the button be:   
   
    <a href="#" class="button-borderd border-indigo">Borderd</a>
   
To change the color of the border, you should change the *border-indigo* option. You have following choices:   
   
>border-red   
>border-pink   
>border-purple   
>border-deeppurple   
>border-indigo   
>border-blue   
>border-lightblue   
>border-cyan   
>border-teal   
>border-green   
>border-lightgreen   
>border-lime   
>border-yellow   
>border-amber   
>border-orange   
>border-deeporange   
>border-brown   
>border-grey   
>border-bluegrey   
>border-black   
   
Have fun with my Pixle-Style Buttons!   

####5.Head-Up
If you don't really understand what did i say above, you can check my [demo page](https://musamdu.tk/button-frame) for help. You can also download the full demo page repo to study. To download the demo page, switch to the "gh-pages" branch like this:   
![switch-branch](./switch-branch.png)   
Or localy, run this command line in the repo:   
   
    git checkout gh-pages
   
##Pay Attention! License!
#####Everything under this repo is under the *Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Licence*, for more details, please read the *"LICENSE.html"* and the *"LICENSE"* file.
#####The *"LICENSE.html"* file is recommanded because it's more user friendly, but you have to open it with a browser.
#####The *"LICENSE"* is the legal code. You can read it with any text editor.

*****

#简体中文手册
##简介
这些就是我针对网络设计的按钮边框。当然，你也可以将它们用于任何其他用途，但这些按钮是专门针对 CSS3 的 *border-image* 设计的。   
   
源文件就在 *"sources"* 文件夹内。这些文件是使用 [Inkscape](inkscape.org) 创建，并手工修改的，使用的是 [SVG](https://zh.wikipedia.org/wiki/%E5%8F%AF%E7%B8%AE%E6%94%BE%E5%90%91%E9%87%8F%E5%9C%96%E5%BD%A2) 格式，是可缩放的。这就是说，你可以随意改变图片的大小，想多大多大，想多小多小，而图片质量不会改变。   
   
我采用的颜色是遵守谷歌的 [Material Design](https://design.google.com/) 设计标准。我使用了其中的20种主要颜色。你可以在[其官网](https://www.google.com/design/spec/style/color.html#color-color-palette)上找到谷歌官方完整的颜色列表。   
   
这些源文件的初始尺寸是12厘米 x 12厘米，并且其中每个方块的大小是1厘米 x 1厘米，所以如果你不想把时间都花在设定图片上，你可能会希望自己生成点阵图片，也可以使用我生成的PNG文件。   

##下载
你有两种方法来进行下载。   

####方法 #1
像这样打开[发布页](https://github.com/MuSamDu/button-frame/releases)：   
![Switch To Release Page](switch-to-release-page.png)   
针对每一版发布，我会使用这样两个标签：   
   
>V#   
>V#-DEMO-PAGE   
   
其中的 *"#"* 号应被换成实际版本号。   
*V#* 标签是用来发布 *master* 分支的。其中包括 SVG 源文件、PNG 点阵图、样式文件、许可条款，以及这份手册。   
*V#-DEMO-PAGE* 标签用来发布演示页面。详情参见下文。   
   
找到最新的版本号，选择好需要的内容。你会发现每个标签下会有两个文件：   
![Files](files.png)   
选择你需要的文件格式，点击它，下载应该很快开始。   

####方法 #2
在这个代码仓库下有两个分支。*"master"* 分支和 *"gh-pages"* 分支。   
*master* 分支中包含 SVG 源文件、PNG 点阵图、样式文件、许可条款，以及这份手册。   
*gh-pages* 分支中包含完整的演示页面代码仓库。详情参见下文。   
像 **[这样](https://github.com/MuSamDu/button-frame#5head-up)**
Choose the branch like **[This](https://github.com/MuSamDu/button-frame#5head-up)** 选择分支。   
接着，你就可以随意下载你想要的内容了。   

##使用
*这些步骤只是我个人的建议，你完全可以按照自己的意愿随意使用，但要遵守许可条款，参见下文。*   

####1.初始化
将我的 *"PNG"* 文件夹复制到与你将要使用的html文件相同的文件夹，起名 *"borders"*。   
   
如果你生成了自己的图片，方法类似，将它们放入 *"borders"* 文件夹，和你要使用的html文件放在一起。接着，从我的 *"PNG"* 文件夹里将 *"borders.css"* 复制出来，和你的图片放在一起。**但是，要将你的图片使用和我一样的方式命名，否则，结果可能不尽人意。**   
   
同时，要将 *"LICENSE"* 文件和 *"LICENSE.html"* 文件也和图片放在一起。为了遵守许可条款，你 **必须** 这样做。参见下文了解有关许可的详细信息。   
   
####2.配置
将如下代码添加到你的 *&lt;/head&gt;* 标签前：   
   
    <link type="text/css" rel="stylesheet" href="borders/borders.css"/>
   
如果你使用了自己的图片，你可能需要对 *"borders"* 文件夹里的 *"borders.css"* 做些改动。   
我将 *border-width* 属性设置为 10px ，当然，你可以进行更改，但最好不要在使用**我的** PNG 文件时进行更改。   
   
####3.设定
你有两种方式。   
第一种方式：   
在 *&lt;/head&gt;* 标签前创建元素：   
   
    <style>

    </style>
   
接着，在 *&lt;style&gt;* 和 *&lt;/style&gt;* 之间粘贴如下代码：   
   
    .button-borderd{
        display: inline-block;
        margin: 10px;
        padding: 10px;
        text-decoration: none;
        background-color: #fff;
        box-shadow: 0px 2px 9px 2px #666666;
        transition: 0.3s;
    }

    .button-borderd:hover{
        text-decoration: none;
        box-shadow: 0px 2px 14px 3px #666666;
        transform: translate(0px,-1px);
    }

    .button-borderd:active{
        text-decoration: none;
        box-shadow: 0px 0px 3px 1px #666666;
        transform: translate(0px,3px);
    }
   
或第二种方式：   
创建文件，命名为 *"buttons.css"*，或打开你现有的样式文件；将上面的代码粘贴至文件末端。为了使这项操作生效，你需要将如下代码粘贴在html文件中 *&lt;/head&gt;* 标签之前：   
   
    <link type="text/css" rel="stylesheet" href="buttons.css"/>
   
若你将代码加入了你自己的样式文件末尾，你可能已经完成了上述操作；如果还没有，就请将上面代码中 *href* 的值改为你的样式文件路径。   
   
**在你知道如何修改的前提下**，你可以修改代码以适应你的需求，并使你的按钮更加美观。   

####4.创建按钮!!!
打开你的html文件，将以下代码粘贴至你想要显示按钮的地方：   
   
    <a href="#" class="button-borderd border-indigo">Borderd</a>
   
若要更改边框颜色，就得更改 *border-indigo* 属性。你有下列可用选项：   
   
>border-red   
>border-pink   
>border-purple   
>border-deeppurple   
>border-indigo   
>border-blue   
>border-lightblue   
>border-cyan   
>border-teal   
>border-green   
>border-lightgreen   
>border-lime   
>border-yellow   
>border-amber   
>border-orange   
>border-deeporange   
>border-brown   
>border-grey   
>border-bluegrey   
>border-black   
   
Have fun with my Pixle-Style Buttons!   

####5.提示
如果你没能很好地理解上面的内容，你就可以参考我的演示页面。你还可以下载完整的演示页代码库，以便研究。若要下载演示页，请这样做，以便切换至 "gh-pages" 分支：   
![switch-branch](./switch-branch.png)   
或在自己的电脑中，在代码库文件夹运行如下命令：   
   
    git checkout gh-pages
   
##注意！许可条款！
#####本代码库下的所有内容均以 *CreativeCommons 知识共享署名-非商业性使用-相同方式共享 4.0 国际许可协议* 授权。参阅 *"LICENSE.html"* 文件和 *"LICENSE"* 文件。
#####推荐阅读*"LICENSE.html"* 文件，因为它可读性比较强，但你需要使用浏览器打开。
#####*"LICENSE"* 文件是具有法律效力的法律条款。你可以使用任何文本编辑器阅读。
